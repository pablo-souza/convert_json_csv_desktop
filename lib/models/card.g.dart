// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'card.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Card _$CardFromJson(Map<String, dynamic> json) => Card(
      id: json['id'] as String,
      desc: json['desc'] as String,
      name: json['name'] as String,
      idList: json['idList'] as String,
      url: json['url'] as String? ?? "",
      labels: (json['labels'] as List<dynamic>?)
              ?.map((e) => Label.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
    )..list = json['list'] == null
        ? null
        : CardList.fromJson(json['list'] as Map<String, dynamic>);

Map<String, dynamic> _$CardToJson(Card instance) => <String, dynamic>{
      'id': instance.id,
      'desc': instance.desc,
      'name': instance.name,
      'url': instance.url,
      'labels': instance.labels,
      'idList': instance.idList,
      'list': instance.list,
    };
