import 'package:convert_json_csv_desktop/models/card_list.dart';
import 'package:convert_json_csv_desktop/models/label.dart';
import 'package:json_annotation/json_annotation.dart';

part 'card.g.dart';

@JsonSerializable()
class Card {
  String id;
  String desc;
  String name;
  String url;
  List<Label> labels;
  String idList;
  CardList? list;

  Card({
    required this.id,
    required this.desc,
    required this.name,
    required this.idList,
    this.url = "",
    this.labels = const [],
  });

  factory Card.fromJson(Map<String, dynamic> json) => _$CardFromJson(json);
  Map<String, dynamic> toJson() => _$CardToJson(this);
}
