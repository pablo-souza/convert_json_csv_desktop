import 'package:convert_json_csv_desktop/models/card.dart';
import 'package:json_annotation/json_annotation.dart';

part 'card_list.g.dart';

@JsonSerializable()
class CardList {
  String id;
  String name;
  List<Card>? cards = [];

  CardList({
    required this.id,
    required this.name,
  });

  factory CardList.fromJson(Map<String, dynamic> json) =>
      _$CardListFromJson(json);

  Map<String, dynamic> toJson() => _$CardListToJson(this);
}
