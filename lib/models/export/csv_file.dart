class CsvFile {
  String list;
  String title;
  String description;
  String points;
  String due;
  String members;
  String labels;
  String cardId;
  String cardUrl;

  CsvFile({
    required this.list,
    required this.title,
    required this.description,
    required this.points,
    required this.due,
    required this.members,
    required this.labels,
    required this.cardId,
    required this.cardUrl,
  });

  String toCsvLine({String separator = ';'}) {
    return '$list$separator$title$separator$description$separator$points$separator$due$separator$members$separator$labels$separator$cardId$separator$cardUrl';
  }

  @override
  String toString() {
    return toCsvLine();
  }
}
