import 'dart:convert';
import 'dart:io';

import 'package:convert_json_csv_desktop/models/card.dart';
import 'package:convert_json_csv_desktop/models/card_list.dart';
import 'package:convert_json_csv_desktop/models/export/csv_file.dart';
import 'package:convert_json_csv_desktop/providers/file_provider.dart';
import 'package:file_picker/file_picker.dart';
import 'package:open_file/open_file.dart';
import 'package:syncfusion_flutter_xlsio/xlsio.dart';

class ImportService {
  final FileProvider provider;
  ImportService({required this.provider});

  Future<void> importFile() async {
    FilePickerResult? file = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['json'],
      dialogTitle: 'Selecionar arquivo para importar',
    );
    if (file == null) return;
    String content = await File(file.files.first.path!).readAsString();
    provider.setImportedFile(File(file.files.first.path!));
    var encoder = const JsonEncoder.withIndent('    ');
    var json = jsonDecode(content);
    provider.setJsonContent(encoder.convert(json));

    var lists = await convertFileToLists(json);
    var cards = await convertCardsFromLists(lists);
    var lines = convertToCsv(cards);
    print(content);
    print(cards);

    for (var line in lines) {
      print(line.toCsvLine());
    }
    provider.setCsvLines(lines);
  }

  Future<List<CardList>> convertFileToLists(Map<String, dynamic> json) async {
    List<Card> cards =
        (json['cards'] as List<dynamic>).map((e) => Card.fromJson(e)).toList();
    List<CardList> lists = (json['lists'] as List<dynamic>)
        .map((e) => CardList.fromJson(e))
        .toList();
    for (var list in lists) {
      list.cards = cards.where((card) => card.idList == list.id).toList();
      for (var card in list.cards ?? []) {
        card.list = list;
      }
    }
    print(cards);
    print(lists);
    return lists;
  }

  Future<List<Card>> convertCardsFromLists(List<CardList> lists) async {
    return lists.map((e) => e.cards ?? []).expand((i) => i).toList();
  }

  List<CsvFile> convertToCsv(List<Card> cards) {
    return cards
        .map((card) => CsvFile(
              list: card.list?.name ?? '',
              title: card.name,
              description: card.desc,
              points: '',
              due: '',
              members: 'members',
              labels: card.labels.join(','),
              cardId: card.id,
              cardUrl: card.url,
            ))
        .toList();
  }

  Future<void> exportCsv() async {
    var path = await FilePicker.platform.saveFile(
      allowedExtensions: ['xlsx'],
      fileName: 'export.xlsx',
    );
    if (path == null) return;
    File file = File(path);
    CsvFile header = CsvFile(
      list: 'List',
      title: 'Title',
      description: 'Description',
      points: 'Points',
      due: 'Due',
      members: 'Members',
      labels: 'Labels',
      cardId: 'Card #',
      cardUrl: 'Card URL',
    );
    final Workbook workbook = Workbook();
    Worksheet sheet = workbook.worksheets[0];
    List<CsvFile> allLines = [header, ...provider.lines];
    /*for (int i = 0; i < allLines.length; i++) {
      var line = allLines[i];
      sheet.insertRow(i );
      sheet.insertColumn(columnIndex)
      //await file.writeAsString('${line.toCsvLine()}\n', mode: FileMode.append);
    }*/

    sheet.importData(
        provider.lines
            .map(
              (line) => ExcelDataRow(
                cells: [
                  ExcelDataCell(
                    columnHeader: 'List',
                    value: line.list,
                  ),
                  ExcelDataCell(
                    columnHeader: 'Title',
                    value: line.title,
                  ),
                  ExcelDataCell(
                    columnHeader: 'Description',
                    value: line.description,
                  ),
                  ExcelDataCell(
                    columnHeader: 'Points',
                    value: line.points,
                  ),
                  ExcelDataCell(
                    columnHeader: 'Due',
                    value: line.due,
                  ),
                  ExcelDataCell(
                    columnHeader: 'Members',
                    value: line.members,
                  ),
                  ExcelDataCell(
                    columnHeader: 'Labels',
                    value: line.labels,
                  ),
                  ExcelDataCell(
                    columnHeader: 'Card #',
                    value: line.cardId,
                  ),
                  ExcelDataCell(
                    columnHeader: 'Card URL',
                    value: line.cardUrl,
                  ),
                ],
              ),
            )
            .toList(),
        1,
        1);
    file.writeAsBytes(workbook.saveAsStream());
    OpenFile.open(file.path);
  }
}
